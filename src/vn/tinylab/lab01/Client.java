package vn.tinylab.lab01;

import java.io.*;
import java.net.Socket;

public class Client {
    private Socket socket;
    private BufferedReader inClient, reader;
    private PrintWriter outClient;
    private String commandLine;

    public Client(String host, int port) {
        try {
            socket = new Socket(host, port);
            inClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outClient = new PrintWriter(socket.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(System.in));
            
            while(true) {
            	commandLine = inClient.readLine();
    			System.out.println(commandLine);
    			
    			sendCommend();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public BufferedReader getInClient() {
        return inClient;
    }

    public PrintWriter getOutClient() {
        return outClient;
    }

    public BufferedReader getReader() {
        return reader;
    }
    
    public void sendCommend() throws IOException {
		String line = inClient.readLine();
		outClient.println(line);
		outClient.flush();
		System.out.println(inClient.readLine());
	}
}
