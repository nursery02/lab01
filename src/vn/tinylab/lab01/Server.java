package vn.tinylab.lab01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class Server {
	private ServerSocket serverSocket;
	private Socket socket;
	private BufferedReader inServer;
	private PrintWriter outServer;
	private String commandLine;
	private String fileName;

	public Server(int port) {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Opening server socket...");
			socket = serverSocket.accept();
			System.out.println("Connected server socket!!!");

			inServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			outServer = new PrintWriter(socket.getOutputStream());
			
			while (true) {
				receiptCommend();
				outServer.println(commandLine);
				outServer.flush();
				System.out.println(commandLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return socket;
	}

	public BufferedReader getInServer() {
		return inServer;
	}

	public PrintWriter getOutServer() {
		return outServer;
	}

	public boolean receiptCommend() throws IOException {
		commandLine = inServer.readLine();
		StringTokenizer token = new StringTokenizer(commandLine, "\t");
		String s = token.nextToken();

		if (s.equalsIgnoreCase("cpu") || s.equalsIgnoreCase("ram")) {
			fileName = token.nextToken();
			commandLine = s;
			outServer.println("Right command");
			outServer.flush();
			outServer.println("1");
			outServer.flush();
			return true;
		} else {
			outServer.println("Wrong command");
			outServer.flush();
			outServer.println("0");
			outServer.flush();
			return false;
		}
	}
	
	public static void main(String[] args) {
		new Server(7777);
	}
}
