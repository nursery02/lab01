package vn.tinylab.lab01;

import java.io.IOException;
import java.util.Scanner;

public class MainTest {
	public static void main(String[] args) throws IOException {
		String host = "127.0.0.1";
		int port = 7777;
		Client client = new Client(host, port);
		
		client.getOutClient().println("cpu");
		client.getOutClient().flush();
		String response = client.getInClient().readLine();
		System.out.println(response);

	}
}
